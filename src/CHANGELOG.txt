## [2.0.0] - not released
 * removed dependency on Python:
    - clustering of visual features done using vlfeat's kdtree implementation (added dependency on vlfeat)
    - frontend is based solely on HTML/CSS/Javascript (previously, it VISE used a python cherrypy based server for generating frontend)
 * Simplified user interface based solely on HTML/CSS/Javascript
    - Many work load transferred from backend server to user's web browser
    - VISE server only generates basic HTML pages containing dynamic data stored as Javascript variable
    - web browser renders the page based on dynamic data contained in Javascript variable
    - VISE servers sends homography matrix and web browser uses Javascript and HTML canvas to generate registered images and required visualisations
 * upgraded code to support compilation in Linux (gcc) and Windows (Visual C++)
 * added tests
    - unit tests for validating basic functionality of modules
    - performance validation test based on Oxford Buildings dataset and its available ground truth
 * images get resized based on user defined max. size (defined in project configuration file)
 * indexing process log saved to a file for reporting
 * ...
 
## [1.0.3] - September 28, 2017
 * international characters (utf-8) are now allowed in image filenames (issue 15)
 * uppercase image filename extensions (PNG, JPG) are now allowed (issue 17)

## [1.0.2] - September 07, 2017
 * docker container for Linux, MacOS and Windows

## [1.0.1] - July 05, 2017
 * fixed issues with load, start, stop and uninstall scripts
 * debugging log saved to $HOME/vgg/vise/log/training.log
 * user images are resized *only* if > 1024px
 * added Refresh button to indexing page to deal with unresponsive pages (temporary fix)

## [1.0.0] - June 15, 2017
 * a web based user interface to index new images

## [0.0.2] - March 16, 2017
 * updated source code and cmake files to allow compilation in Mac OS
 * mostly used by @Giles Bergel (Seebibyte project ambassador) to demonstrate potential users

## [0.0.1] - September 25, 2017
 * the original relja_retrival codebase
 * developed by Relja Arandjelovic during his DPhil / Postdoc at the Visual Geometry Group, Department of Engineering Science, University of Oxford.
